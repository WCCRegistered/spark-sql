package com.etc

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

/**
  * @Auther: Wangcc
  * @Date: 2018/8/19 16:39
  * @Description:
  */
object DataFrameOperationScala {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("DataFrameCreate")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    val df = sqlContext.read.json("hdfs://spark1:9000/students.json")

    df.show()
    df.printSchema()
    df.select("name").show()
    df.select(df("name"), df("age") + 1).show()
    df.filter(df("age") > 18).show()
    df.groupBy("age").count().show()
  }
}
