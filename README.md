## 学习目录介绍<br>
>* 1.Spark SQL-DataFrame的使用
>* 2.Spark SQL-使用反射方式将RDD转换为DataFrame
>* 3.Spark SQL-使用编程方式将RDD转换为DataFrame
>* 4.Spark SQL-数据源之通用的load和save操作
>* 5.Spark SQL-Parquet数据源之使用编程方式加载数据
>* 6.Spark SQL-Parquet数据源之自动分区推断
>* 7.Spark SQL-Parquet数据源之合并元数据
>* 8.Spark SQL-JSON数据源复杂综合案例实战
>* 9.Spark SQL-Hive数据源复杂综合案例实战(重要)
>* 10.Spark SQL-JDBC数据源复杂综合案例实战
>* 11.Spark SQL-内置函数以及每日uv、销售额统计案例实战
>* 12.Spark SQL-开窗函数以及top3销售额统计案例实战    
>* 13.Spark SQL-UDF自定义函数实战
>* 14.Spark SQL-UDAF自定义聚合函数实战
>* 15.Spark SQL-核心源码深度剖析（DataFrame lazy特性、Optimizer优化策略等）
>* 16.Spark SQL-延伸知识之Hive On Spark
>* 17.Spark SQL-与Spark Core整合之每日top3热点搜索词统计案例实战


#### 创建DataFrame
```
DataFrame，可以理解为是，以列的形式组织的，分布式的数据集合。它其实和关系型数据库中的表非常类似，但是底层做了很多的优化。DataFrame可以通过很多来源进行构建，包括：结构化的数据文件，
```

* Java版本：
```
JavaSparkContext sc = ...; 
SQLContext sqlContext = new SQLContext(sc);
DataFrame df = sqlContext.read().json("hdfs://spark1:9000/students.json");
df.show();
```
* Scala版本：
```
val sc: SparkContext = ...
val sqlContext = new SQLContext(sc)
val df = sqlContext.read.json("hdfs://spark1:9000/students.json")
df.show()
```

#### Spark SQL：JSON数据源

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
### Spark SQL：Hive数据源复杂综合案例实战(重要)
* Hive数据源实战
Spark SQL支持对Hive中存储的数据进行读写。操作Hive中的数据时，必须创建HiveContext，而不是SQLContext。HiveContext继承自SQLContext，但是增加了在Hive元数据库中查找表，以及用HiveQL语法编写SQL的功能。除了sql()方法，HiveContext还提供了hql()方法，从而用Hive语法来编译sql。

* 使用HiveContext，可以执行Hive的大部分功能，包括创建表、往表里导入数据以及用SQL语句查询表中的数据。查询出来的数据是一个Row数组。

* 将hive-site.xml拷贝到spark/conf目录下，将mysql connector拷贝到spark/lib目录下
```

HiveContext sqlContext = new HiveContext(sc);
sqlContext.sql("CREATE TABLE IF NOT EXISTS students (name STRING, age INT)");
sqlContext.sql("LOAD DATA LOCAL INPATH '/usr/local/spark-study/resources/students.txt' INTO TABLE students");
Row[] teenagers = sqlContext.sql("SELECT name, age FROM students WHERE age<=18").collect();
```
#### UDF自定义函数实战
* UDF：User Defined Function。用户自定义函数

#### UDAF自定义函数实战
* UDAF：User Defined Aggregate Function。用户自定义聚合函数。是Spark 1.5.x引入的最新特性。

* UDF，其实更多的是针对单行输入，返回一个输出
这里的UDAF，则可以针对多行输入，进行聚合计算，返回一个输出，功能更加强大
